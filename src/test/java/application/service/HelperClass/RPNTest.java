package application.service.HelperClass;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class RPNTest {

    private RPN rpn;

    @Before
    public void setUP(){
        rpn = new RPN();
    }

    @Test
    public void testCalculating() {
        String expr = "2+2";
        double expected_res = 4;
        double actual_res = rpn.doAllWork(expr);
        Assert.assertEquals(expected_res, actual_res, 0.1);
    }

}
