package application.service.services;
import application.entity.Account;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AccountManagerTest {

    @Before
    public void setUP(){
        AccountManager.addAccount(new Account("", "name", "e@mail.com", "password"));
        AccountManager.addAccount(new Account("", "No_Name", "q@mail.com", "qwerty123"));
    }

    @Test
    public void testRegistrationCheck() {
        boolean expected = false;
        boolean actual = AccountManager.isRegistrated("name", "qwerty123");
        Assert.assertEquals(expected, actual);
        expected = true;
        actual = AccountManager.isRegistrated("q@mail.com", "qwerty123");
        Assert.assertEquals(expected, actual);
    }





    @After
    public void qwe() {
        AccountManager.clear(AccountManagerTest.class);
    }
}
