package application.entity;

import javax.persistence.*;
import javax.persistence.Entity;
import java.io.Serializable;


@Entity
@Table(name = "accounts")
public class Account implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "content")
    private String content;
    @Column(name = "account_name")
    private String accountName;
    @Column(name = "email")
    private String email;
    @Column(name = "password")
    private String password;

    //TODO: полезные данные об аккаунте, распиханные по классам. где? в казахстане

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public Account() {}
    public Account(String content, String accName, String eMail, String password) {
        this.content = content;
        this.accountName = accName;
        this.password = password;
        this.email = eMail;
    }

}
