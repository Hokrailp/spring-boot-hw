package application.entity;

public class Expression {
// пока бесполезный класс, мб буду его использовать, что бы парсить выражение, но пока нет
    private String expression;
    private double result;

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public double getResult() {
        return result;
    }

    public void setResult(double result) {
        this.result = result;
    }

    public Expression() {
        expression = "";
        result = 0;
    }
    public Expression(String expr, double res) {
        expression = expr;
        result = res;
    }

}
