package application.service;

import application.entity.Account;

public interface LoginService {
    //мы можем зайти(совпал логин/почта и пароль)
    boolean isValid(Account account);
    boolean isValid(String nameOrEmail, String password);
}
