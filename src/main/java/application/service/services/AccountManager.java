package application.service.services;


import application.entity.Account;

import java.util.LinkedList;
import java.util.List;

public class AccountManager {

    private static List<Account> allAccounts;

    static {
        allAccounts = new LinkedList<>();
    }

    public static List<Account> getAllAccounts() {
        return allAccounts;
    }

    public static Account getLast() {
        return allAccounts.get(allAccounts.size()-1);
    }

    public static boolean addAccount(Account account) {
        Account.incrementIndex();
        return allAccounts.add(account);
    }

    public static boolean isRegistrated(Account account) {
        return allAccounts.stream().anyMatch(a -> a.getAccountName().equals(account.getAccountName()) || a.getEmail().equals(account.getEmail()));
    }
    // UNIT TEST
    public static boolean isRegistrated(String nameOrEmail, String password) {
        return allAccounts.stream().anyMatch(a ->
            ((nameOrEmail.equals(a.getEmail())) || (nameOrEmail.equals(a.getAccountName())))
            &&(password.equals(a.getPassword()))
        );
    }

    public static Account findAppUserByEmail(String email) {
        return allAccounts.stream().filter(a -> a.getEmail().equals(email)).findAny().get();
    }

    public static Account findAppUserByUserName(String name) {
        return allAccounts.stream().filter(a -> a.getAccountName().equals(name)).findAny().get();
    }



    public static void clear(Class clas) {
        //класс теста не получу(((
        allAccounts.clear();
    }
}
