package application.service.services;

import application.entity.Expression;
import application.service.CalcService;
import application.service.HelperClass.RPN;
import org.springframework.stereotype.Service;

@Service
public class CalcServiceImpl implements CalcService {

    public Expression evaluate(String expression) {
        RPN rpn = new RPN(expression);
        return rpn.getExpression();
    }
    public Expression evaluate(Expression expression) {
        RPN rpn = new RPN(expression.getExpression());
        return rpn.getExpression();
    }

    public CalcServiceImpl() {}
}
