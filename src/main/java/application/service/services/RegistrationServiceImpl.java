package application.service.services;

import application.database.AccountRepository;
import application.entity.Account;
import application.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
public class RegistrationServiceImpl implements RegistrationService {


    private AccountRepository accountRepository;
    @Autowired
    public RegistrationServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }


    @Override
    public Account addAccount(Account account) {
        return this.accountRepository.save(account);
    }

    public List<Account> getAllAccounts() {
        List<Account> employees = new LinkedList<>();
        this.accountRepository.findAll().forEach(employees::add);
        return employees;
    }

}
