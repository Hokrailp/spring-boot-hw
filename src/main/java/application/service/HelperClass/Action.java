package application.service.HelperClass;

// Only '+''-' '/' '*' '^' and sin, cos, tan, atan, asin, acos, ln
public class Action extends Stuff {

    public boolean isTrigonometryF(String actq) {
        //only sin, cos, tan, atan, asin, acos
        if (actq.equals("sin") ||
                actq.equals("cos") ||
                actq.equals("tan") ||
                actq.equals("atan") ||
                actq.equals("asin") ||
                actq.equals("acos"))
            return true;
        return false;
    }

    public Action(String act) {
        action = act;
        if (action.equals("^"))
            priority = 5;
        else if (isTrigonometryF(action) || action.equals("ln")) {
            priority = 4;
        }
        else if ((action.equals("*")) || (action.equals("/"))) {
            priority = 3;
        }
        else if ((action.equals("+")) || (action.equals("-")) || action.equals("%")) {
            priority = 2;
            beforeNum = false;
        }
        else
            priority = 1; //только у скобок
        // 0 только у чисел
    }

    public Action(String act, boolean flag) {
        action = act;
        priority = 2;
        beforeNum = flag;
    }

    protected int length;

    protected boolean beforeNum;
}
