package application.service;

import application.entity.Expression;

public interface CalcService {

    Expression evaluate(String expression);
    Expression evaluate(Expression expression);
}
