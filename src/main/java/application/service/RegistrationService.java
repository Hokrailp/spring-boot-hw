package application.service;

import application.entity.Account;

import java.util.List;

public interface RegistrationService {
    //добавляю(регаю) аккаунт
    Account addAccount(Account account);
    List<Account> getAllAccounts();
}
