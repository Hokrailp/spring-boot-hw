package application.controller;

import application.entity.Expression;
import application.service.services.CalcServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class CalclController {

    private CalcServiceImpl calcService;

    @Autowired
    public CalclController(CalcServiceImpl greetingService) {
        this.calcService = greetingService;
    }

    @GetMapping("/calc")
    public String calculateForm(Model model) {
        model.addAttribute("expression", new Expression());
        return "toCalculate";
    }

    @PostMapping("/calc")
    public String calculationSubmit(@ModelAttribute Expression expression, Model model) {
        model.addAttribute("done",calcService.evaluate(expression));
        return "result";
    }

}

