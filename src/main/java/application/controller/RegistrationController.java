package application.controller;

import application.database.AccountRepository;
import application.entity.Account;
import application.service.services.AccountManager;
import application.service.services.RegistrationServiceImpl;
import application.validator.AppAccountValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

@Controller
public class RegistrationController {

    private RegistrationServiceImpl registrationService;
    private AppAccountValidator appAccountValidator;

    @Autowired
    RegistrationController(AccountRepository accountRepository) {
        registrationService = new RegistrationServiceImpl(accountRepository);
        appAccountValidator = new AppAccountValidator();
    }

    // Set a form validator
    @InitBinder
    protected void initBinder(WebDataBinder dataBinder) {
        // Form target
        Object target = dataBinder.getTarget();
        if (target == null) {
            return;
        }
        System.out.println("Target=" + target);

        if (target.getClass() == Account.class) {
            dataBinder.setValidator(appAccountValidator);
        }
        // ...
    }


    @GetMapping("/reg")
    public String registrationForm(Model model) {
        model.addAttribute("toReg", new Account());
        return "registration";
    }

    // @Validated: To ensure that this Form
    // has been Validated before this method is invoked.
    @PostMapping("/reg")
    public String registrationSubmit(@ModelAttribute Account account) {
        registrationService.addAccount(account);
        return "redirect:/registerSuccessful";
    }

    @RequestMapping("/registerSuccessful")
    public String viewRegisterSuccessful(Model model) {
        model.addAttribute("acc",AccountManager.getLast());
        return "registerSuccessfulPage";
    }

}
