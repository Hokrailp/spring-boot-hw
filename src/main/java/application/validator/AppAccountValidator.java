package application.validator;

// Не использую уже
import application.entity.Account;
import application.service.services.AccountManager;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class AppAccountValidator implements Validator {

    // common-validator library.
    private EmailValidator emailValidator = EmailValidator.getInstance();

    // The classes are supported by this validator.
    @Override
    public boolean supports(Class<?> clazz) {
        return clazz == Account.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
        Account account = (Account) target;

        // Check the fields of AppUserForm.
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "accName", "NotEmpty.account.accName");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty.account.email");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty.account.password");

        if (!this.emailValidator.isValid(account.getEmail())) {
            // Invalid email.
            errors.rejectValue("email", "Pattern.account.email");
        } else {
            Account dbUser = AccountManager.findAppUserByEmail(account.getEmail());
            if (dbUser != null) {
                // Email has been used by another account.
                errors.rejectValue("email", "Duplicate.account.email");
            }
        }

        if (!errors.hasFieldErrors("accName")) {
            Account dbUser = AccountManager.findAppUserByUserName(account.getAccountName());
            if (dbUser != null) {
                // Username is not available.
                errors.rejectValue("accName", "Duplicate.account.accName");
            }
        }
    }

}
